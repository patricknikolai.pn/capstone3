import React, { useState, useEffect } from "react";
import UserOrder from "../components/UserOrder";

export default function OrderPage() {
  const [orderList, setOrderList] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4000/order/retrieveOrder", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrderList(data);
        console.log(data)
      });
  }, []);

  return (
    <div>
      <UserOrder orderData={orderList} />
    </div>
  );
}
