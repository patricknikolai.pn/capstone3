import React from 'react';
import {Link} from 'react-router-dom';

function pageNotFound(){
	return(
		<div>
			<h1>Page not found</h1>
			<h2>kindly click this link to return to <Link to='/'> Home </Link>.</h2>
		</div>
		)
}

export default pageNotFound