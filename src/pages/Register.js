import React,{useState,useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useHistory} from 'react-router-dom';
export default function Register(){

	const history = useHistory();
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [verifyPassword,setVerifyPassword] = useState("");
	const [registerButton,setRegisterButton] = useState(false)

	useEffect(() =>{
		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword){
			setRegisterButton(true)
		} else{
			setRegisterButton(false)
		}
	},[email,password,verifyPassword,registerButton])


	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName:firstName,
				lastName:lastName,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(result =>{
		console.log(result)

		if(/*!result.emailExist*/result === true){

			Swal.fire({
				title: "yaay",
				icon: 'success',
				text: "You have successfully registered"
			})
			history.push('/login')
			
		} else{
			Swal.fire({
				title: "Meron na",
				icon: 'error',
				text: "Email already exist"
			})
			setFirstName('')
			setLastName('')
			setEmail('')
			setPassword('')
			setVerifyPassword('')
		}
	})
}

			return(
	<>
	<div class="register">
		<h1>Register</h1>
		<Form onSubmit={e => registerUser(e)}>
		<Form.Group>
			<Form.Label>First Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
		</Form.Group>
		<Form.Group>
			<Form.Label>Last Name:</Form.Label>
			<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
		</Form.Group>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" required value={email} onChange={e => setEmail(e.target.value)} />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e => setPassword(e.target.value)}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control type="password" placeholder="Verify password" required value={verifyPassword}onChange={e => setVerifyPassword(e.target.value)}/>
			</Form.Group>
			{registerButton ? 
			<Button variant="primary" type="submit">Submit</Button>
			
				:
			<Button variant="primary" type="submit" disabled>Submit</Button>
			}
			
		</Form>
	</div>
	</>
	)
}