import React,{useState,useEffect,useContext} from 'react';
import {Container} from 'react-bootstrap';
import UserContext from '../userContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import Products from '../components/Products';



export default function ProductsPage(){

	

{	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch("http://localhost:4000/product/all")
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setAllProducts(data);
		})
	}

	useEffect(() =>{
		fetchData()
	}, [])

	return(
		<Container>
		{user.isAdmin ? <AdminView fetchData={fetchData} productData={allProducts} /> : <UserView productData ={allProducts}/>}
		</Container>
		)
}}