import React, {useState,useEffect,useContext} from 'react';

import {Container,Card,Button} from 'react-bootstrap';

import { Link, useHistory,useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';



export default function SpecificCourse(){

	const history = useHistory();

	const { user } = useContext(UserContext);

	const [name,setName] = useState('')
	const [description,setDescription] = useState('')
	const [price,setPrice] = useState('')
	const [addItems,setAddItems] = useState(1)

	const { productId } = useParams();

	useEffect(()=>{
		fetch(`http://localhost:4000/product/getSingle/${productId}`)
		.then(res => res.json())
		.then(data =>{
			
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [])


	useEffect(()=>{
		if(addItems < 1){
			setAddItems(1)
		}
	}, [addItems])

	const checkout = () =>{

		fetch(`http://localhost:4000/order/checkout/${productId}`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				quantity:addItems
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data != null){
				Swal.fire({
					title: 'Successfully Purchased',
					icon: 'success',
					text: 'you have successfully purchased the product.'
				})
				history.push('/products')
			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again!.'
				})
			}
		})
}


	return(
			<Container>
				<Card className="mt-5">
					<Card.Header className="bg-dark text-white text-center">
						<h4>{name}</h4>
					</Card.Header>
					<Card.Body>
						<Card.Text>{description}</Card.Text>
						<h6>Price: PHP {price}</h6>
					</Card.Body>
					<Card.Footer>

					{
						user.accessToken !== null ?
						<>
						<input type = "number" value={addItems} onChange={(e)=>setAddItems(e.target.value)}/>

						<Button variant="primary" block onClick={()=>checkout(productId)}>
							Checkout
						</Button>
						</>
						:
						<Link className="btn btn-warning btn-block" to="/login">
						Login to Checkout
						</Link>
					}
						
					</Card.Footer>
				</Card>
			</Container>
		)
}
