import React from 'react';

export default function Home(){
	return (
		<div>
			<section id="hero" class="d-flex ">
			    <div class="container">
			            <div class="row">
			                <div class="col-lg-12 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
			                    <h1><b style={{color:'black'}}>Make your mornings better and nights longer with </b><span class="typing">Bean there, done that.</span></h1>
			                    <br />
			                    <h2 style={{color:'black'}}>Sourced locally, made with quality.</h2>
			                    <div class="d-flex justify-content-center justify-content-lg-start">
			                        <a href="#about" class="btn-get-started scrollto">Scoop <i class="bi bi-arrow-down"></i></a>
			                        <a href="/Products" class="btn-get-started scrollto mx-4">Menu <i class="bi bi-arrow-right"></i></a>
			                    </div>
			                </div>
			            </div>
			    </div>  
			</section>

			
			<section class="about" id="about">
			    <div class="container aos-init aos-animate" data-aos="fade-up"> 
			        <div class="section-title">
			            <p>About Us</p>
			        </div>
			        <div class="row content justify-content-center text-center" id="about">
			            <div class="col-lg-4 pt-lg-0 text-center">
			                <p>
			                    Sourced locally, made with quality. Our handcrafted Coldbrew coffee uses all-natural ingredients with no artificial flavorings.
			                </p>
			                <a href="#strawberry" class="btn-learn-more">Cold Brew <i class="bi bi-arrow-down"></i></a>
			            </div> 
			            <div class="col-lg-4 pt-4 pt-lg-0 text-center">
			                <p>
			                    Coffee beans sourced from formars all over the country.
			                    We offer coffee beans varying from over 11 different taste and flavors.
			                </p>
			                <a href="#strawberry" class="btn-learn-more">Coffee Beans <i class="bi bi-arrow-down"></i></a>
			            </div>
			        </div> 
			    </div>
			</section> 

			<section id="hero">
			    <div class="row" id="stickyArabica">
			        <div class="col-lg-6">
			            <img id="arabica" src="/assets/img/arabica long.png" class="img-fluid"/>
			        </div>
			        <blockquote id="arabicaDesc" style={{position:'absolute', width:'50%', marginTop:'15%', marginLeft:'50%'}}>
			            <h1 style={{fontSize: '2.3em'}}>Classic Coffee Beans</h1>
			            <h2 style={{fontSize: '1.3em'}}>Enjoyed by many coffee connoisseur, arabica beans has a nutty, fruity, sweet, and soft taste. It is surely a best buy for those who want pleasure from their cup.</h2>
			            <div class="d-flex justify-content-center justify-content-lg-start">
			                <a href="milkshake" class="btn-get-started scrollto">See more</a>
			            </div>
			        </blockquote>
			    </div>
			    <div class="row" id="stickyRobusta">
			        <blockquote id="robustaDesc" style={{position:'absolute', width:'50%', marginTop:'15%', marginRight:'50%'}}>
			            <h1 class="right" style={{fontSize: '2.3em'}}>Flavored Coffee Beans</h1>
			            <h2 class="right" style={{fontSize: '1.3em'}}>
			                One of the pioneers, Robusta is the second most popular coffee on the world market. Robusta coffee has a strong and deep flavor. Robusta beans contains the highest caffeine content among the other beans.
			            </h2>
			            <div class="d-flex justify-content-center justify-content-lg-end">
			                <a href="milkshake" class="btn-get-started scrollto">See more</a>
			            </div>
			        </blockquote>
			        <div class="col-lg-6 order-1 order-lg-1 hero-img-right">
			            <img id="robusta" src="/assets/img/robusta halved2.png" class="img-fluid right"/>
			        </div>
			    </div>
			    <div class="row" id="stickycb">
			        <blockquote id="cbDesc" style={{position:'relative', marginBottom:'50%', marginLeft:'10%', marginRight:'10%', marginTop:'10%'}}>
			            <h1 style={{fontSize: '2.3em'}}>Cold Brew</h1>
			            <h2 style={{fontSize: '1.3em'}}>
			                Our handcrafted Coldbrew coffee uses all-natural ingredients with no artificial flavorings.
			                
			                <br />
			                Choose from over 5 different flavors, one of which is sure to get your taste bud's preference. 
			            </h2>
			            <div class="d-flex justify-content-center justify-content-lg-start">
			                <a href="milkshake" class="btn-get-started scrollto">See more</a>
			            </div>
			        </blockquote> 
			            <img id="cb" src="/assets/img/test.png" class="img-fluid"/> 
			    </div>
			</section>    

{/*Footer*/}
			<footer id="footer">
			    <div class="footer-top">
			        <div class="container">
			            <div class="row">

			                <div class="col-lg-4 col-md-6 footer-contact">
			                    <h3>Bean there, done that.</h3>
			                    <p>
			                        Pacita 2,<br/>
			                        San Pedro, Laguna,<br/>
			                        Philippines <br/><br/>
			                        <strong>Phone:</strong> 09289015988<br/>
			                        <strong>Email:</strong> beantheredt@gmail.com
			                        <br/>
			                    </p>
			                </div>
			                <div class="col-lg-4 col-md-6 footer-links">
			                    <h4>Links</h4>
			                    <ul>
			                        <li><i class="bx bx-chevron-right"></i> <a href="index">Home</a></li>
			                        <li><i class="bx bx-chevron-right"></i> <a href="index#about">About us</a></li>
			                        <li><i class="bx bx-chevron-right"></i> <a href="menu">Menu</a></li>
			                        <li><i class="bx bx-chevron-right"></i> <a href="#">Order Now!</a></li>
			                    </ul>
			                </div>
			                <div class="col-lg-4 col-md-6 footer-links">
			                    <h4>Our Social Networks</h4>
			                    <p>Contact us through our Facebook and Instagram below! Feel Free to Message us! <br/><br/> Make your day better with BTDT.</p>
			                    <div class="social-links mt-3">
			                        <a href="http://fb.me/beantheredt" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
			                        <a href="https://www.instagram.com/beantheredt" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>

			    <div class="container footer-bottom clearfix">
			        <div class="copyright">
			            &copy; Copyright <strong><span>Bean there, Done that.</span></strong>. All Rights Reserved
			        </div>
			        <div class="credits">

			        </div>
			    </div>
			</footer>
		</div>



	)
}