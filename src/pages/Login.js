import React,{useState,useEffect,useContext} from 'react';


import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import userContext from '../userContext';
import {Redirect,useHistory} from 'react-router-dom';

export default function Login(){

	const {user,setUser} = useContext(userContext)
	const history = useHistory();
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [loginButton,setLoginButton] = useState(false);

	useEffect(() =>{
		if(email !== '' && password !== ''){
			setLoginButton(true)
		} else{
			setLoginButton(false)
		}
	},[email,password,loginButton])



	function loginUser(e){
		e.preventDefault();

		//log in
		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken})

				Swal.fire({
					title: 'Sike',
					icon: 'success',
					text: 'Thank you for logging in'
				})

				//get details of user
				fetch('http://localhost:4000/users/',{
					headers:{
						Authorization:`Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result =>{
				console.log(result)

				if(result.isAdmin === true){

					localStorage.setItem('email', result.email)
					localStorage.setItem('isAdmin',result.isAdmin)

					setUser({
						email:result.email,
						isAdmin:result.isAdmin
					})
					
					history.push('/products')
				}else{
					localStorage.setItem('email', result.email)
					localStorage.setItem('isAdmin',result.isAdmin)

					setUser({
						email:result.email,
						isAdmin:result.isAdmin
					})
					history.push('/')
				}

				})

			}else {
				Swal.fire({
					title: 'oooopppsss!',
					icon: 'error',
					text: 'Something went wrong. Check your credentials'
				})
			}
			setEmail('')
			setPassword('')

		})

	
	}


	if(user.email !== null){
		return <Redirect to="/" />
	}
	return(
	<>
		<h1>Login</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter email" required value={email} onChange={e => setEmail(e.target.value)} />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" required value={password} onChange={e => setPassword(e.target.value)}/>
			</Form.Group>
			{loginButton ? 
			<Button variant="primary" type="submit">Login</Button>
			
				:
			<Button variant="primary" type="submit" disabled>Login</Button>
			}
		</Form>
	</>
	)
}