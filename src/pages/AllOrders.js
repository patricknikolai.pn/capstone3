import React, { useState, useEffect } from "react";
import UserOrder from "../components/UserOrder";

export default function OrderPage() {
  const [orderList, setOrderList] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4000/order/allOrders", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        if(data){
            setOrderList(data);
        } else if(data.Error){
            console.log(data.Error)
        }
       
        console.log(data)
      });
  }, []);

  return (
    <div>
      <UserOrder orderData={orderList} />
    </div>
  );
}


