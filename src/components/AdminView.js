import React, { useState, useEffect} from "react";
import Products from "./Products"
import { Table, Button, Modal, Form } from "react-bootstrap";
import Swal from 'sweetalert2';

export default function AdminView (props) {
    const { productData, fetchData } = props;

    const [productId,setProductId] = useState('')
    const [products, setProducts] = useState([]);

    const [name,setName] = useState('');
    const [description,setDescription] = useState('');
    const [price,setPrice] = useState(0)

    const [showAdd,setShowAdd] = useState(false)
    const [showEdit,setShowEdit] = useState(false)

    const openAdd =() => setShowAdd(true)
    const closeAdd =() => setShowAdd(false)


    const openEdit = (product) => {

            setProductId(product._id)
            setName(product.name)
            setDescription(product.description)
            setPrice(product.price)
    
        setShowEdit(true)
};


    const closeEdit =() => {
        setShowEdit(false)
        setName('')
        setDescription('')
        setPrice(0)    
    }



    useEffect(() => {
        const productsArray = productData.map(product => {
            return(
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>{`P${product.price.toLocaleString()}.00`}</td>
                    <td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
                    <td>
                        <Button variant="primary" size="sm" onClick={() => openEdit(product)}>
                            Update
                        </Button>

                        {
                            product.isActive
                            ?
                            <Button variant ="danger" size ="sm" onClick={() => archiveToggle
                                (product._id,product.isActive)}>
                                Disable
                            </Button>
                            :
                            <Button variant="success" size="sm" onClick={() => activateToggle
                                (product._id,product.isActive)}>
                                Enable
                            </Button>
                        }
                    </td>
                </tr>

            )
        })
        setProducts(productsArray);
    }, [productData])


    //function for add product
    const addproduct = (e) =>{
        e.preventDefault();

        fetch('http://localhost:4000/product/add',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name:name,
                description:description,
                price:price
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data)
            if(data === true){
                fetchData()
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'product successfully added'
                })

                setName('')
                setDescription('')
                setPrice(0)

                closeAdd()
            } else{
                    fetchData()
                  Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again'
                })

            }
        })
    }



    //update a product
    const editproduct = (e,productId) =>{
        e.preventDefault()

        fetch(`http://localhost:4000/product/update/${productId}`,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`

            },
            body: JSON.stringify({
                name:name,
                description:description,
                price:price
            })

        })
        .then(res => res.json())
        .then(data =>{
            if(data === true){
                fetchData()
                Swal.fire({
                    title:'Success',
                    icon:'success',
                    text:'product updated successfully'
                })
                closeEdit()
            }else {
                fetchData()
                 Swal.fire({
                    title:'Something went wrong',
                    icon:'error',
                    text:'Please try again'
                })

            }
        })
    }

    //archive
  const archiveToggle = (productId, isActive) => {
          fetch(`http://localhost:4000/product/archive/${productId}`, {
              method: "DELETE",
              headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${localStorage.getItem("accessToken")}`
              },
              body: JSON.stringify({
                  isActive: isActive
              })
          })
          .then(res => res.json())
          .then(data => {
              if (data) {
                  fetchData();
                  Swal.fire({
                      title: "Success",
                      icon: "success",
                      text: "product successfully archived!"
                  })
              } else {
                  fetchData();
                  Swal.fire({
                      title: "Something went wrong!!",
                      icon: "error",
                      text: "Please try again."
                  })
              }
          })
      }
    //activate toggle

    const activateToggle = (productId, isActive) => {
            fetch(`http://localhost:4000/product/enable/${productId}`, {
                method: "PATCH",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("accessToken")}`
                },
                body: JSON.stringify({
                    isActive: isActive
                })
            })
            .then(res => res.json())
            .then(data => {
                if (data) {
                    fetchData();
                    Swal.fire({
                        title: "Success",
                        icon: "success",
                        text: "product successfully Enabled"
                    })
                } else {
                    fetchData();
                    Swal.fire({
                        title: "Something went wrong!!",
                        icon: "error",
                        text: "Please try again."
                    })
                }
            })
        }
      
    


    return(
        <>
            <div className="text-center my-4">
                <h2>Admin Dashboard</h2>
                <div className="d-flex justify-content-center">
                    <Button variant="primary" onClick={openAdd}>Add New product</Button>
                </div>
            </div>
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>{products}</tbody>
            </Table>
            {/*ADD MODAL*/}

            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => addproduct(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type = "text" placeholder="Enter Title of a product" required value={name}
                            onChange={e => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type = "text" placeholder="Enter Description" required value={description}
                            onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control type = "number" required value={price} 
                            onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

        {/*Edit MODAL*/}

            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editproduct(e , productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type = "text" placeholder="Enter Title of a product" required value={name}
                            onChange={e => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type = "text" placeholder="Enter Description" required value={description}
                            onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Price</Form.Label>
                            <Form.Control type = "number" required value={price} 
                            onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

        </>
    )
}
