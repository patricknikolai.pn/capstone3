import React, { useEffect, useState } from "react";
import Products from "./Products";
import {Col} from 'react-bootstrap';

export default function UserView ({productData}) {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const productsArray = productData.map(products => {
            if (products.isActive) {
                return <Products key={products._id} productsProp={products} />;
            } else {
                return null;
            }
        })
        setProducts(productsArray);
    }, [productData])

    return(
    <>

            {products}
            </>
    )
}