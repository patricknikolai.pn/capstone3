//counter.js

//effect hooks, events and forms
//what does useEffect do? by using this hook, you tell react that your component needs to do something after render. react will remember the function you passed

import React, {useState, useEffect} from 'react';

//bootstrap
import {Container, Button} from 'react-bootstrap';

export default function Counter(){

	const [count, setCount] = useState(0)

	//using the useEffect requires two arguments: a function and an array of variables
	//when the value of a variable in a given array is changed the given function will be triggered
	useEffect(()=>{
		document.title = `You clicked ${count} times`
	}, [count])

	return(
		<Container>
			<p>You clicked {count}</p>
			<Button variant="primary" onClick={()=> setCount(count + 1)}>
				Click Me
			</Button>
		</Container>
	)
}