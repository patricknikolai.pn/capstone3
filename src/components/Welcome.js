import React from 'react';

export default function Welcome({name,Age}){
	return(
		<>
			<h1>Hello,{name}, Age: {Age}</h1>
			<p>Welcome to our Course Booking</p>
		</>
		)
}