import React from "react";
import { Link } from "react-router-dom";

export default function Orders({ orderProps }) {
  const {
    productName,
    price,
    productId,
    quantity,
    description,
  } = orderProps;

  const totalAmount = price * quantity;
  return (
    <div className="card-container">
      <div className="body">
        <h5 className="product-name">{productName}</h5>
        <p className="description">{description}</p>
        <p className="price">₱ {price}</p>
        <small>Total ₱{totalAmount}</small>

      </div>
    </div>
  );
}