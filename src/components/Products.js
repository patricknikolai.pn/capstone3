import React, {useState} from 'react';

import {Card,Button,Row,Col} from 'react-bootstrap';
import Arabica from '../Images/5.png';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
export default function Products({productsProp}){

	const {_id, name,description,price } = productsProp;

	
	
	return(

	<>
		<div class="services" id="services">
		        <div class="serv-content">
		            <div class="card col-md-12">
		            <h2 class="title">{name}</h2>
		                <div class="box">
		                    <i class="fas fa-coffee"></i>
		                    <div class="text">{price}</div>
		                    <p>{description}</p>
		                    <Link className="btn btn-light" to={`/products/${_id}`}>Details</Link>
		                </div>
		          
		         </div>
		     </div>
		 </div>
	</>
		)
}

