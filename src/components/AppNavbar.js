
import React,{useContext} from 'react';
import { Link, NavLink,useHistory } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import userContext from '../userContext';
import Images from '../Images/t.png';
import {Container} from 'react-bootstrap';







export default function AppNavbar(){

	const history = useHistory();

	const {user,unsetUser} = useContext(userContext);
	console.log(user)

	const logout = () =>{
		unsetUser();
		//push
		history.push('/login')
	}

	let rightNav = (user.email !== null) ?
	(
		<>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</>
	) 
	:
	(
		<>
			<Nav.Link class="getstarted" as={NavLink} to="/login">Login</Nav.Link>
			<Nav.Link class="getstarted" as={NavLink} to="/register">Register</Nav.Link>
		</>
	)
	return(

		<Navbar id="navbar" class="navbar" expand="lg">
			<Navbar.Brand as={Link} to="/">
				{<img className="img-fluid" width="75px" src={Images}/>}
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-Navbar-nav"/>
			<Navbar.Collapse id="basic-Navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link class="getstarted" as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link class="getstarted" as={NavLink} to="/Products">Products</Nav.Link>
					<Nav.Link class="getstarted" as={NavLink} to="/orderHistory">Orders</Nav.Link>
				</Nav>
				<Nav className="ml-auto">
					{rightNav}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}