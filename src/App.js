import React,{useState}from 'react'
import './App.css';
//routing
import {BrowserRouter as Router,Redirect,Route,Switch} from 'react-router-dom';
//components
import AppNavbar from './components/AppNavbar';
//pages
import OrderHistory from './pages/orderHistory';
import Home from './pages/Home';
import productPage from './pages/productPage';
import Register from './pages/Register';
import Login from './pages/Login';
import Notfound from './pages/Notfound';
//bootstrap

import SpecificProduct from './pages/SpecificProduct';
import userContext from './userContext';
import AllOrders from './pages/AllOrders';
function App() {

const [user, setUser] = useState({
    //getItem() method returns value of the specified Storage Object item.
    email: localStorage.getItem('email'),
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'

  })


const unsetUser = () =>{
  localStorage.clear();
  setUser({
    email:null,
    accessToken:null,
    isAdmin:null
  })
};


  return (
    <React.Fragment>
      <userContext.Provider value={{user,setUser,unsetUser}}>
        <Router>
          < AppNavbar />
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/products" component={productPage}/>
              <Route exact path="/login">
              {user.email ? <Redirect to ="/"/> : <Login/>}
              </Route>
              <Route exact path="/register">
              {user.email ? <Redirect to ="/"/> : <Register/>}
              </Route>
              <Route exact path="/products/:productId" component={SpecificProduct}/>
              <Route exact path="/orderHistory">
              {user.isAdmin ? <AllOrders/> : <OrderHistory/>}
              </Route>
              <Route component={Notfound}/>
            </Switch>
        </Router>
      </userContext.Provider>
    </React.Fragment>
  );
}

export default App;
