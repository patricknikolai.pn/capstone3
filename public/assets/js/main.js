 (function() { 

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }
 
  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })
 

  /**
   * Preloader
   */
  let preloader = select('#preloader');
  if (preloader) {
    window.addEventListener('load', () => {
      preloader.remove()
    });
  } 
 
     

  /**
   * Animation on scroll
   */
  window.addEventListener('load', () => {
    AOS.init({
      duration: 1000,
      easing: "ease-in-out",
      once: true,
      mirror: false
    });
  });

    /**
    * Scroll Magic
    */

var tl = new TimelineMax({ onUpdate: updatePercentage }); 
const controller = new ScrollMagic.Controller();

tl.from('#arabica', .5, { x: -450, opacity: 1 }); 
tl.from('#arabicaDesc', 1, { x: 200, opacity: 0, ease: Power4.easeInOut }, "=-.7");



const scene = new ScrollMagic.Scene({
    triggerElement: "#stickyArabica",
    triggerHook: "onLeave",
    duration: "100%"
})
    .setPin("#stickyArabica")
    .setTween(tl)
    .addTo(controller); 

    var typed = new Typed(".typing", {
        strings: ["Bean there, Done that!","a cup of coffee!"],
        typeSpeed: 100,
        backSpeed: 60,
        loop: true
    });

var tl2 = new TimelineMax({ onUpdate: updatePercentage });  

tl2.from('#robusta', .5, { x: 450, opacity: 1 }); 
tl2.from('#robustaDesc', 1, { x: 200, opacity: 0, ease: Power4.easeInOut }, "=-.7");

const scene2 = new ScrollMagic.Scene({
    triggerElement: "#stickyRobusta",
    triggerHook: "onLeave",
    duration: "100%"
})
    .setPin("#stickyRobusta")
    .setTween(tl2)
    .addTo(controller); 

    var tl3 = new TimelineMax({ onUpdate: updatePercentage });

    tl3.from('#cb', .5, { y: 450, opacity: 0 }); 
    tl3.from('#cbDesc', 1, { y: 200, opacity: 0, ease: Power4.easeInOut }, "=-.7");

    const scene3 = new ScrollMagic.Scene({
        triggerElement: "#stickycb",
        triggerHook: "onLeave",
        duration: "100%"
    })
        .setPin("#stickycb")
        .setTween(tl3)
        .addTo(controller);
        
function updatePercentage() {
    //percent.innerHTML = (tl.progress() *100 ).toFixed();
    tl.progress();
    console.log(tl.progress());
}
$(window).scroll(function(){
    /*Sticky Navbar*/
    if(this.scrollY > 20){
        $('.navbar').addClass("sticky");
    }else{
        $('.navbar').removeClass("sticky");
    }
    
});
})()